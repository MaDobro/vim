" Enable italic. Default: 1
" let g:kolor_italic=1

" Enable bold. Default: 1
" let g:kolor_bold=1

" Enable underline. Default: 0
" let g:kolor_underlined=0

" Gray 'MatchParen' color. Default: 0
" let g:kolor_alternative_matchparen=0

" White foreground 'MatchParen' color that might work better with some terminals. Default: 0
" let g:kolor_inverted_matchparen=0

"-------------------------------------------------------------------------------
" Colors
"-------------------------------------------------------------------------------

" colorscheme solarized
"set background=dark

"-------------------------------------------------------------------------------
" set guifont=Source\ Code\ Pro\ for\ Powerline\ 20
set guifont=Lucida_Console:h22:cANSI:qDRAFT

set guioptions=agit 
set nocompatible 
syntax enable 
filetype plugin indent on 
set autoindent 
set expandtab 
set hidden
set encoding=utf-8
set hlsearch
set history=1000
set number
set relativenumber
set scrolloff=7
set wildmenu
set foldcolumn=1

set undofile
set undodir=$HOME/viundo/
set undolevels=1000

map <f2> :w<cr>
imap <f2> <esc>:w<cr>l
vmap <f2> <esc>:w<cr>

"  Tastaturkürzel definieren
let mapleader=","
" Rechtschreibprüfung an/aus
map <leader>s :setlocal spell!<cr>

map <leader>y :%y+<cr>:%y<cr>

" vim beenden
map q :q<cr>

" Schnell wechseln zwischen Fenstern
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k


" PlugIns installieren
call plug#begin('~/.vim/plugged')

Plug 'rakr/vim-one'
Plug 'kien/ctrlp.vim' 
Plug 'SirVer/ultisnips'
Plug 'vim-airline/vim-airline' 
Plug 'vim-airline/vim-airline-themes'

"   " Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
"   Plug 'junegunn/vim-easy-align'
"
"   " Any valid git URL is allowed
"   Plug 'https://github.com/junegunn/vim-github-dashboard.git'
"
"   " Multiple Plug commands can be written in a single line using | separators
"   Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'
"
"   " On-demand loading
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
"   Plug 'tpope/vim-fireplace', { 'for': 'clojure' }
"
"   " Using a non-master branch
"   Plug 'rdnetto/YCM-Generator', { 'branch': 'stable' }
"
"   " Using a tagged release; wildcard allowed (requires git 1.9.2 or above)
"   Plug 'fatih/vim-go', { 'tag': '*' }
"
"   " Plugin options
"   Plug 'nsf/gocode', { 'tag': 'v.20150303', 'rtp': 'vim' }
"
"   " Plugin outside ~/.vim/plugged with post-update hook
"   Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"
"   " Unmanaged plugin (manually installed and updated)
"   Plug '~/my-prototype-plugin'
"
"   " Initialize plugin system
call plug#end() 

" set runtimepath^=~/.vim/bundle/ctrlp.vim

colorscheme one
set background=dark " for the dark version
" set background=light " for the light version

